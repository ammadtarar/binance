//
//  MainVC.swift
//  Binance
//
//  Created by Ammad Amjad Tarar on 2018/8/19.
//  Copyright © 2018 Ammad Amjad Tarar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MainVC: UIViewController , UIScrollViewDelegate{

    @IBOutlet weak var bnb: UIView!
    @IBOutlet weak var btc: UIView!
    @IBOutlet weak var eth: UIView!
    @IBOutlet weak var usdt: UIView!
    
    @IBOutlet weak var btBnb: UIButton!
    @IBOutlet weak var btBtc: UIButton!
    @IBOutlet weak var btEth: UIButton!
    @IBOutlet weak var btUsdt: UIButton!
    
    @IBOutlet weak var indicator: UIView!
    
    @IBOutlet weak var scroller: UIScrollView!
    
    var currentFragmentIndex = 0
    
    var screenWidth : CGFloat = 0
    @IBOutlet weak var v: UIView!
    
    var bnbVC : ListVC!
    var btcVC : ListVC!
    var ethVC : ListVC!
    var usdtVC : ListVC!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bnbVC = self.storyboard?.instantiateViewController(withIdentifier: "ListVC") as! ListVC
        bnbVC.parentVC = self
        btcVC = self.storyboard?.instantiateViewController(withIdentifier: "ListVC") as! ListVC
        btcVC.parentVC = self
        ethVC = self.storyboard?.instantiateViewController(withIdentifier: "ListVC") as! ListVC
        ethVC.parentVC = self
        usdtVC = self.storyboard?.instantiateViewController(withIdentifier: "ListVC") as! ListVC
        usdtVC.parentVC = self
        initTabs()
        setupContainers()
        getData()
    }
    
    
    func initTabs(){
        setStatus(btn: btBnb, active: true)
        setStatus(btn: btBtc, active: false)
        setStatus(btn: btEth, active: false)
        setStatus(btn: btUsdt, active: false)
    }
    
    func setStatus(btn : UIButton , active : Bool){
        btn.setTitleColor(active ? ViewsAttrsController.colorWithHexString(hexString: Colors.THEME_YELLOW) : UIColor.white , for: .normal)
    }
    
    func goToPage(atIndex : Int){
        let x = self.view.frame.width * CGFloat(atIndex)
        scroller.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func setIndicatorForPage(atIndex : CGFloat){
        UIView.animate(withDuration: 0.3, animations: {
            self.indicator.frame.origin.x = CGFloat((self.screenWidth/4)*atIndex)
        })
    }
    
    func getData(){
        Alamofire.request("https://www.binance.com/exchange/public/product").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                var d : [JSON] = json["data"].array!
                
                var btcList : [JSON] = []
                var etnList : [JSON] = []
                var bnbList : [JSON] = []
                var usdtList : [JSON] = []
                
                for var i in (0..<d.count){
                    var obj = d[i]
                    var baseAsset = obj["quoteAsset"].stringValue
                    if baseAsset.elementsEqual("BTC"){
                        btcList.append(obj)
                    }else if baseAsset.elementsEqual("ETH"){
                        etnList.append(obj)
                    }else if baseAsset.elementsEqual("BNB"){
                        bnbList.append(obj)
                    }else if baseAsset.elementsEqual("USDT"){
                        usdtList.append(obj)
                    }
                }
                
                self.btcVC.setData(list: btcList)
                self.btcVC.refreshControl.endRefreshing()
                self.bnbVC.setData(list: bnbList)
                self.bnbVC.refreshControl.endRefreshing()
                self.ethVC.setData(list: etnList)
                self.ethVC.refreshControl.endRefreshing()
                self.usdtVC.setData(list: usdtList)
                self.usdtVC.refreshControl.endRefreshing()
                
                
                
                break
                
            case .failure(let error): break
                
            }
            
            
            
        }
    }
    
    
    func setupContainers(){
        self.scroller.delegate = self
        screenWidth = self.view.frame.width
        let viewHeight = self.view.frame.height
        
        let cons = self.v.frame.height
        var z : CGFloat = CGFloat(viewHeight) - CGFloat(cons) - 60
        
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            z = z - 70
        }
        
        indicator.frame.origin.y = 58
        indicator.frame.origin.x = 0
        indicator.frame.size = CGSize(width: screenWidth/4, height: 2)
        
        self.scroller.frame = CGRect(x : 0 , y : cons , width :  screenWidth , height : z)
        
        self.scroller.isPagingEnabled = true
        
        let size = CGSize(width : screenWidth , height : z)
        self.bnb.frame.size = size
        self.bnb.frame.origin.y = 0
        self.bnb.frame.origin.x = 0
        bnbVC.setHeight(size: size)
        self.bnb.addSubview(bnbVC.view)
        
        self.btc.frame.size = size
        self.btc.frame.origin.y = 0
        self.btc.frame.origin.x = screenWidth
        btcVC.setHeight(size: size)
        self.btc.addSubview(btcVC.view)
        
        self.eth.frame.size = size
        self.eth.frame.origin.y = 0
        self.eth.frame.origin.x = (2 * screenWidth)
        ethVC.setHeight(size: size)
        self.eth.addSubview(ethVC.view)
        
        self.usdt.frame.size = size
        self.usdt.frame.origin.y = 0
        self.usdt.frame.origin.x = (3 * screenWidth)
        usdtVC.setHeight(size: size)
        self.usdt.addSubview(usdtVC.view)
        
        self.scroller.showsHorizontalScrollIndicator = false
        self.scroller.showsVerticalScrollIndicator = false
        self.scroller.contentSize.width = screenWidth * 4
        
    }
  
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber : Int = Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
        switch pageNumber {
        case 0:
            setBnbActive()
            break
        case 1:
            setBtcActive()
            break
        case 2:
            setEthActive()
            break
        case 3:
            setUsdtActive()
            break
        default:
            setBnbActive()
            break
        }
        
    }
    
    func setBnbActive(){
        setStatus(btn: btBnb, active: true)
        setStatus(btn: btBtc, active: false)
        setStatus(btn: btEth, active: false)
        setStatus(btn: btUsdt, active: false)
        setIndicatorForPage(atIndex: 0)
    }
    
    func setBtcActive(){
        setStatus(btn: btBnb, active: false)
        setStatus(btn: btBtc, active: true)
        setStatus(btn: btEth, active: false)
        setStatus(btn: btUsdt, active: false)
        setIndicatorForPage(atIndex: 1)
    }
    
    func setEthActive(){
        setStatus(btn: btBnb, active: false)
        setStatus(btn: btBtc, active: false)
        setStatus(btn: btEth, active: true)
        setStatus(btn: btUsdt, active: false)
        setIndicatorForPage(atIndex: 2)
    }
    
    func setUsdtActive(){
        setStatus(btn: btBnb, active: false)
        setStatus(btn: btBtc, active: false)
        setStatus(btn: btEth, active: false)
        setStatus(btn: btUsdt, active: true)
        setIndicatorForPage(atIndex: 3)
    }

    @IBAction func onClickBnb(_ sender: Any) {
        setBnbActive()
        goToPage(atIndex: 0)
    }
    
    @IBAction func onClickBtc(_ sender: Any) {
        setBtcActive()
        goToPage(atIndex: 1)
    }
    
    @IBAction func onClickEth(_ sender: Any) {
       setEthActive()
        goToPage(atIndex: 2)
    }
    
    @IBAction func onClickUsdt(_ sender: Any) {
        setUsdtActive()
        goToPage(atIndex: 3)
    }
    
    
    
}

