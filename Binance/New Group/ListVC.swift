//
//  ListVC.swift
//  Binance
//
//  Created by Ammad Amjad Tarar on 2018/8/19.
//  Copyright © 2018 Ammad Amjad Tarar. All rights reserved.
//

import UIKit
import SwiftyJSON

class ListVC: UIViewController , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var listView: UITableView!
    let refreshControl = UIRefreshControl()
    var data : [JSON] = []
    var parentVC : MainVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listView.delegate = self
        listView.dataSource = self
        listView.tableFooterView = UIView()
        setPullToRefresh()
    }
    
    func setHeight (size : CGSize){
        self.view.frame.size = size
    }
    
    func setPullToRefresh(){
        refreshControl.addTarget(self, action: #selector(refresh(_:)) , for: .valueChanged)
        if #available(iOS 10.0, *) {
            listView.refreshControl = refreshControl
        } else {
            listView.backgroundView = refreshControl
        }
    }
    
    func setData(list  : [JSON]){
        self.data.removeAll()
        self.data = list
        self.listView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        if (parentVC != nil){
            parentVC.getData()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
        let index = indexPath.row
        let obj = self.data[indexPath.row] 
        cell.tvName.text = obj["baseAsset"].stringValue
        cell.tvType.text = "/ " + obj["quoteAsset"].stringValue
        cell.tvVol.text = "Vol " + obj["volume"].stringValue
        cell.tvQty.text = obj["close"].stringValue
        cell.tvPrice.text = "$ " + String(obj["tradedMoney"].floatValue)
        return cell
    }
   
}
