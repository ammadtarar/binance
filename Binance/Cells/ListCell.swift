//
//  ListCell.swift
//  Binance
//
//  Created by Ammad Amjad Tarar on 2018/8/19.
//  Copyright © 2018 Ammad Amjad Tarar. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    
    
    @IBOutlet weak var tvName: UILabel!
    @IBOutlet weak var tvType: UILabel!
    @IBOutlet weak var tvVol: UILabel!
    @IBOutlet weak var tvQty: UILabel!
    @IBOutlet weak var tvPrice: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
